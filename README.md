# Craft Magic

## Purpose
This repository's purpose is to make a badass Plugin for doing anything you
could possibly want to do in Minecraft quickly and easily with only one tool:
Magic. I'm not sure what this will look like yet and am totally open to
questions, suggestions, and contribution. Hopefully by the time this is done,
it will be a powerful tool: a VIM or EMACS of Minecraft.

> Also, this literally just started so give it some time before you slam me:)
