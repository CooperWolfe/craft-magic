package io.github.craftmagic.services.sources;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.model.sources.MagicSource;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public interface SourceService {
  List<MagicSource> getSources();
  MagicSource addSource(Location<World> location);
  MagicSource removeSource(int index);
  void clearSources();
  void selectCircle(int radius, Vector3i normal);
  void fill(BlockType type);
  void deselect();
  void selectSphere(int radius);
  void outline(BlockType type);
  void selectSquare(int radius, Vector3i normal);
  void selectCube(int radius);
  void expand(int distance, Vector3i direction);
  void wall(Vector3i direction, BlockType block);
  void walls(BlockType type);
  void roof(BlockType type);
  void floor(BlockType type);
  void replace(BlockType type, BlockType with);
  void half(Vector3i direction);
  void show();
  void hide();
}
