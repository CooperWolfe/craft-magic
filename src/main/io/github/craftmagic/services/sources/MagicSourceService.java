package io.github.craftmagic.services.sources;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.model.sources.MagicSource;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;

public class MagicSourceService implements SourceService {

  private ArrayList<MagicSource> sources;
  private boolean show;

  public MagicSourceService() {
    this.sources = new ArrayList<>();
    this.show = false;
  }

  @Override
  public List<MagicSource> getSources() {
    return (ArrayList<MagicSource>) this.sources.clone();
  }

  @Override
  public MagicSource addSource(Location<World> location) {
    MagicSource source = new MagicSource(location, show);
    for (MagicSource src : this.sources)
      if (src.getOrigin().equals(location))
        return null;
    this.sources.add(source);
    return source;
  }

  @Override
  public MagicSource removeSource(int index) {
    this.sources.get(index).deconstruct();
    return this.sources.remove(index);
  }

  @Override
  public void clearSources() {
    this.sources.forEach(MagicSource::deconstruct);
    this.sources.clear();
  }

  @Override
  public void selectCircle(int radius, Vector3i normal) {
    this.sources.forEach(src -> src.selectCircle(radius, normal));
  }

  @Override
  public void fill(BlockType type) {
    for (MagicSource source : this.sources) source.fill(type);
  }

  @Override
  public void deselect() {
    for (MagicSource source : this.sources) source.resetSelection();
  }

  @Override
  public void selectSphere(int radius) {
    this.sources.forEach(src -> src.selectSphere(radius));
  }

  @Override
  public void outline(BlockType type) {
    this.sources.forEach(src -> src.outline(type));
  }

  @Override
  public void selectSquare(int radius, Vector3i normal) {
    this.sources.forEach(src -> src.selectSquare(radius, normal));
  }

  @Override
  public void selectCube(int radius) {
    this.sources.forEach(src -> src.selectCube(radius));
  }

  @Override
  public void expand(int distance, Vector3i direction) {
    this.sources.forEach(src -> src.expand(distance, direction));
  }

  @Override
  public void wall(Vector3i direction, BlockType type) {
    this.sources.forEach(src -> src.wall(type, direction));
  }

  @Override
  public void walls(BlockType type) {
    wall(Vector3i.UNIT_X, type);
    wall(Vector3i.UNIT_X.mul(-1), type);
    wall(Vector3i.UNIT_Z, type);
    wall(Vector3i.UNIT_Z.mul(-1), type);
  }

  @Override
  public void roof(BlockType type) {
    this.sources.forEach(src -> src.roof(type));
  }

  @Override
  public void floor(BlockType type) {
    this.sources.forEach(src -> src.floor(type));
  }

  @Override
  public void replace(BlockType type, BlockType with) {
    this.sources.forEach(src -> src.replace(type, with));
  }

  @Override
  public void half(Vector3i direction) {
    this.sources.forEach(src -> src.half(direction));
  }

  @Override
  public void show() {
    this.show = true;
    this.sources.forEach(MagicSource::show);
  }

  @Override
  public void hide() {
    this.show = false;
    this.sources.forEach(MagicSource::hide);
  }
}
