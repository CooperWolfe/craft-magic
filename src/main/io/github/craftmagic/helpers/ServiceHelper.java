package io.github.craftmagic.helpers;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.service.ProvisioningException;

public final class ServiceHelper {
  public static <T> T getService(Class<T> clazz) throws ProvisioningException {
    return Sponge.getServiceManager()
      .provide(clazz)
      .orElseThrow(() ->
        new ProvisioningException(clazz)
      );
  }
}
