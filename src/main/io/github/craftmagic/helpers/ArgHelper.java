package io.github.craftmagic.helpers;

import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.model.potions.MagicPotionEffectType;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.CommandFlags;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.text.Text;

public final class ArgHelper {

  // Primitives
  public static CommandElement integer(String key) {
    return GenericArguments.onlyOne(GenericArguments.integer(Text.of(key)));
  }
  public static CommandElement integer(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.integer(Text.of(key)))
      : integer(key);
  }

  // Catalogued types
  public static CommandElement magicBlock(String key) {
    return GenericArguments.onlyOne(GenericArguments.catalogedElement(Text.of(key), MagicBlockType.class));
  }
  public static CommandElement magicBlock(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.catalogedElement(Text.of(key), MagicBlockType.class))
      : magicBlock(key);
  }
  public static CommandElement potion(String key) {
    return GenericArguments.onlyOne(GenericArguments.catalogedElement(Text.of(key), MagicPotionEffectType.class));
  }
  public static CommandElement potion(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.catalogedElement(Text.of(key), MagicPotionEffectType.class))
      : potion(key);
  }

  // Choices
  public static CommandElement direction(String key) {
    return GenericArguments.onlyOne(GenericArguments.choices(Text.of(key), OrientationHelper.getDirections()));
  }
  public static CommandElement direction(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.choices(Text.of(key), OrientationHelper.getDirections()))
      : direction(key);
  }
  public static CommandElement plane(String key) {
    return GenericArguments.onlyOne(GenericArguments.choices(Text.of(key), OrientationHelper.getPlanes()));
  }
  public static CommandElement plane(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.choices(Text.of(key), OrientationHelper.getPlanes()))
      : plane(key);
  }
  public static CommandElement side(String key) {
    return GenericArguments.onlyOne(GenericArguments.choices(Text.of(key), OrientationHelper.getSides()));
  }
  public static CommandElement side(String key, boolean optional) {
    return optional
      ? GenericArguments.optional(GenericArguments.choices(Text.of(key), OrientationHelper.getSides()))
      : side(key);
  }

  // Collections
  public static CommandElement optional(CommandElement... elements) {
    return GenericArguments.optional(GenericArguments.seq(elements));
  }

  // Flags
  public static CommandElement flags(Flag... flags) {
    CommandFlags.Builder builder = GenericArguments.flags();
    for (Flag flag : flags)
      builder = flag instanceof ValueFlag
        ? builder.valueFlag(((ValueFlag) flag).getCommandElement(), flag.getSpecs())
        : builder.flag(flag.getSpecs());
    return builder.buildWith(GenericArguments.none());
  }
  public static Flag flag(String... specs) {
    return new Flag(specs);
  }
  public static ValueFlag magicBlockFlag(String key, String... specs) {
    return new ValueFlag(GenericArguments.catalogedElement(Text.of(key), MagicBlockType.class), specs);
  }
  public static ValueFlag vector3dFlag(String key, String... specs) {
    return new ValueFlag(GenericArguments.vector3d(Text.of(key)), specs);
  }
  public static class Flag {
    private final String[] specs;

    public String[] getSpecs() {
      return specs;
    }

    public Flag(String... specs) {
      this.specs = specs;
    }
  }
  public static class ValueFlag extends ArgHelper.Flag {
    private final CommandElement commandElement;

    public CommandElement getCommandElement() {
      return commandElement;
    }

    public ValueFlag(CommandElement commandElement, String... specs) {
      super(specs);
      this.commandElement = commandElement;
    }
  }
}
