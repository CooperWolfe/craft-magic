package io.github.craftmagic.helpers;

import com.flowpowered.math.vector.Vector3i;
import com.sun.istack.internal.NotNull;
import org.spongepowered.api.entity.living.player.Player;

import java.util.HashMap;
import java.util.Map;

public final class OrientationHelper {

  public static Vector3i getRelativeAxis(@NotNull Player player, @NotNull Vector3i direction) {
    assert direction.length() == 1;
    assert Math.abs(direction.getX()) == 1 ^ Math.abs(direction.getY()) == 1 ^ Math.abs(direction.getZ()) == 1;
    if (Math.abs(direction.getY()) == 1) return direction;

    Vector3i perspectiveAxis = getPerspectiveAxis(player);
    if (perspectiveAxis.getX() > 0) return new Vector3i(-direction.getZ(), 0, direction.getX());
    if (perspectiveAxis.getX() < 0) return new Vector3i(direction.getZ(), 0, -direction.getX());
    if (perspectiveAxis.getZ() > 0) return direction.mul(-1);
    return direction;
  }

  public static Map<String, Vector3i> getDirections() {
    Map<String, Vector3i> directions = new HashMap<>(10);
    directions.put("right", Vector3i.UNIT_X);
    directions.put("left", Vector3i.UNIT_X.mul(-1));
    directions.put("up", Vector3i.UNIT_Y);
    directions.put("down", Vector3i.UNIT_Y.mul(-1));
    directions.put("back", Vector3i.UNIT_Z);
    directions.put("backward", Vector3i.UNIT_Z);
    directions.put("away", Vector3i.UNIT_Z);
    directions.put("forth", Vector3i.UNIT_Z.mul(-1));
    directions.put("forward", Vector3i.UNIT_Z.mul(-1));
    directions.put("here", Vector3i.UNIT_Z.mul(-1));
    return directions;
  }

  public static Map<String, Vector3i> getSides() {
    Map<String, Vector3i> directions = new HashMap<>(6);
    directions.put("right", Vector3i.UNIT_X);
    directions.put("left", Vector3i.UNIT_X.mul(-1));
    directions.put("top", Vector3i.UNIT_Y);
    directions.put("bottom", Vector3i.UNIT_Y.mul(-1));
    directions.put("front", Vector3i.UNIT_Z);
    directions.put("back", Vector3i.UNIT_Z.mul(-1));
    return directions;
  }

  public static Map<String, Vector3i> getPlanes() {
    Map<String, Vector3i> axes = new HashMap<>(3);
    axes.put("xz", Vector3i.UNIT_Y);
    axes.put("xy", Vector3i.UNIT_Z);
    axes.put("yz", Vector3i.UNIT_X);
    return axes;
  }

  private static Vector3i getPerspectiveAxis(@NotNull Player player) {
    double yaw = Math.toRadians(player.getHeadRotation().getY()) - Math.PI;
    double x = Math.sin(yaw); x = x == 0 ? 0.001 : x;
    double z = -Math.cos(yaw); z = z == 0 ? 0.001 : z;
    return Math.abs(x) > 0.707106781
      ? new Vector3i(x / Math.abs(x), 0, 0)
      : new Vector3i(0, 0, z / Math.abs(z));
  }

}