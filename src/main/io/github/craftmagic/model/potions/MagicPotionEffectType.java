package io.github.craftmagic.model.potions;

import org.spongepowered.api.CatalogType;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.util.annotation.CatalogedBy;

@CatalogedBy(MagicPotionEffectTypes.class)
public interface MagicPotionEffectType extends CatalogType {

  /**
   * Gets the potion effect type
   *
   * @return the potion effect type
   */
  PotionEffectType getPotionEffectType();

}
