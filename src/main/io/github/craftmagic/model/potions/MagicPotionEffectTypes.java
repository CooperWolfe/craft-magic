package io.github.craftmagic.model.potions;

import io.github.craftmagic.registries.potions.MagicPotionEffectTypeRegistryModule;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.effect.potion.PotionEffectTypes;

@SuppressWarnings("unused")
public final class MagicPotionEffectTypes {

  public static final MagicPotionEffectType ABSORPTION = createMagicPotionEffect(PotionEffectTypes.ABSORPTION);

  public static final MagicPotionEffectType BLINDNESS = createMagicPotionEffect(PotionEffectTypes.BLINDNESS);

  public static final MagicPotionEffectType FIRE_RESISTANCE = createMagicPotionEffect(PotionEffectTypes.FIRE_RESISTANCE);

  public static final MagicPotionEffectType GLOWING = createMagicPotionEffect(PotionEffectTypes.GLOWING);

  public static final MagicPotionEffectType HASTE = createMagicPotionEffect(PotionEffectTypes.HASTE);

  public static final MagicPotionEffectType HEALTH_BOOST = createMagicPotionEffect(PotionEffectTypes.HEALTH_BOOST);

  public static final MagicPotionEffectType HUNGER = createMagicPotionEffect(PotionEffectTypes.HUNGER);

  public static final MagicPotionEffectType INSTANT_DAMAGE = createMagicPotionEffect(PotionEffectTypes.INSTANT_DAMAGE);

  public static final MagicPotionEffectType INSTANT_HEALTH = createMagicPotionEffect(PotionEffectTypes.INSTANT_HEALTH);

  public static final MagicPotionEffectType INVISIBILITY = createMagicPotionEffect(PotionEffectTypes.INVISIBILITY);

  public static final MagicPotionEffectType JUMP_BOOST = createMagicPotionEffect(PotionEffectTypes.JUMP_BOOST);

  public static final MagicPotionEffectType LEVITATION = createMagicPotionEffect(PotionEffectTypes.LEVITATION);

  public static final MagicPotionEffectType LUCK = createMagicPotionEffect(PotionEffectTypes.LUCK);

  public static final MagicPotionEffectType MINING_FATIGUE = createMagicPotionEffect(PotionEffectTypes.MINING_FATIGUE);

  public static final MagicPotionEffectType NAUSEA = createMagicPotionEffect(PotionEffectTypes.NAUSEA);

  public static final MagicPotionEffectType NIGHT_VISION = createMagicPotionEffect(PotionEffectTypes.NIGHT_VISION);

  public static final MagicPotionEffectType POISON = createMagicPotionEffect(PotionEffectTypes.POISON);

  public static final MagicPotionEffectType REGENERATION = createMagicPotionEffect(PotionEffectTypes.REGENERATION);

  public static final MagicPotionEffectType RESISTANCE = createMagicPotionEffect(PotionEffectTypes.RESISTANCE);

  public static final MagicPotionEffectType SATURATION = createMagicPotionEffect(PotionEffectTypes.SATURATION);

  public static final MagicPotionEffectType SLOWNESS = createMagicPotionEffect(PotionEffectTypes.SLOWNESS);

  public static final MagicPotionEffectType SPEED = createMagicPotionEffect(PotionEffectTypes.SPEED);

  public static final MagicPotionEffectType STRENGTH = createMagicPotionEffect(PotionEffectTypes.STRENGTH);

  public static final MagicPotionEffectType UNLUCK = createMagicPotionEffect(PotionEffectTypes.UNLUCK);

  public static final MagicPotionEffectType WATER_BREATHING = createMagicPotionEffect(PotionEffectTypes.WATER_BREATHING);

  public static final MagicPotionEffectType WEAKNESS = createMagicPotionEffect(PotionEffectTypes.WEAKNESS);

  public static final MagicPotionEffectType WITHER = createMagicPotionEffect(PotionEffectTypes.WITHER);

  public MagicPotionEffectTypes() {}

  private static MagicPotionEffectType createMagicPotionEffect(PotionEffectType potionEffectType) {
    return MagicPotionEffectTypeRegistryModule.register(new MagicPotionEffectType() {
      @Override
      public PotionEffectType getPotionEffectType() {
        return potionEffectType;
      }

      @Override
      public String getId() {
        return potionEffectType.getId().replace("minecraft:", "");
      }

      @Override
      public String getName() {
        return potionEffectType.getName().replace("minecraft:", "");
      }
    });
  }

}
