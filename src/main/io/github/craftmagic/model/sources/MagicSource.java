package io.github.craftmagic.model.sources;

import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class MagicSource {

  private Location<World> origin;
  private HashSet<Location<World>> selection;
  private BlockType type;
  private boolean show;


  public MagicSource(Location<World> origin, boolean show) {
    this.origin = origin;
    this.type = origin.getBlockType();
    this.selection = new HashSet<>();
    addToSelection(origin);
    this.show = show;
    if (show) origin.setBlockType(BlockTypes.BEACON);
  }


  public Location<World> getOrigin() {
    return this.origin;
  }

  public BlockType getType() {
    return this.type;
  }

  public void move(Vector3i offset) {
    // Move origin
    this.origin.setBlockType(type);
    this.origin = this.origin.add(offset);
    this.type = this.origin.getBlockType();
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);

    // Move selection
    HashSet<Location<World>> clone = (HashSet<Location<World>>) this.selection.clone();
    this.selection = new HashSet<>();
    clone.forEach(loc -> addToSelection(loc.add(offset)));
  }

  public void show() {
    this.origin.setBlockType(BlockTypes.BEACON);
    this.show = true;
  }

  public void hide() {
    this.origin.setBlockType(this.type);
    this.show = false;
  }

  public void selectSquare(int r, Vector3i n) {
    assert(r > 0); assert(n.length() == 1);
    assert(n.getX() == 1 ^ n.getY() == 1 ^ n.getZ() == 1);

    for (int i = -r; i <= r; ++i)
      for (int j = -r; j <= r; ++j)
        if (n.equals(Vector3i.UNIT_X)) addToSelection(this.origin.add(0, j, i));
        else if (n.equals(Vector3i.UNIT_Y)) addToSelection(this.origin.add(j, 0, i));
        else addToSelection(this.origin.add(j, i, 0));
  }

  public void selectCube(int r) {
    assert(r > 0);
    for (int i = -r; i <= r; ++i)
      for (int j = -r; j <= r; ++j)
        for (int k = -r; k <= r; ++k)
          addToSelection(this.origin.add(k, j, i));
  }

  public void selectCircle(int r, Vector3i n) {
    assert(r > 0); assert(n.length() == 1);
    assert(n.getX() == 1 ^ n.getY() == 1 ^ n.getZ() == 1);
    for (int i = 0; i <= Math.sqrt(2)*r/2; ++i) {
      for (int j = i; j < r; ++j) {
        Vector2d x = new Vector2d(j, i);
        if (x.distance(Vector2d.ZERO) > r) break;
        addCirclePoint(x, n);
      }
    }
  }

  public void selectSphere(int r) {
    assert(r > 0);
    for (int i = 0; i < Math.sqrt(2)*r/2; ++i) {
      for (int j = i; j < r; ++j) {
        Vector3i x = new Vector3i(0, j, i);
        if (x.distance(Vector3i.ZERO) > r) break;
        for (int k = j; k < r; ++k) {
          x = new Vector3i(k, j, i);
          if (x.distance(Vector3i.ZERO) > r) break;
          addSpherePoint(x);
        }
      }
    }
  }

  public void expand(int distance, Vector3i direction) {
    assert distance > 0; assert direction.length() == 1;
    assert Math.abs(direction.getX()) == 1 ^ Math.abs(direction.getY()) == 1 ^ Math.abs(direction.getZ()) == 1;

    Map<Vector3i, Location<World>> directionMost = getClosestTo(direction);
    directionMost.forEach((key, value) -> {
      for (int i = 1; i <= distance; ++i)
        addToSelection(value.add(direction.mul(i)));
    });
  }

  public void resetSelection() {
    this.selection = new HashSet<>();
    addToSelection(this.origin);
  }

  public void fill(BlockType type) {
    this.type = type;
    for (Location<World> block : this.selection)
      try { block.setBlockType(type); } catch (Exception ignored) {}

    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void outline(BlockType type) {
    this.selection.forEach(loc -> {
      if (
        this.selection.contains(loc.add(1, 0, 0)) &&
        this.selection.contains(loc.add(0, 1, 0)) &&
        this.selection.contains(loc.add(0, 0, 1)) &&
        this.selection.contains(loc.add(-1, 0, 0)) &&
        this.selection.contains(loc.add(0, -1, 0)) &&
        this.selection.contains(loc.add(0, 0, -1))
      ) return;
      if (loc.equals(this.origin)) this.type = type;
      loc.setBlockType(type);
    });
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void roof(BlockType type) {
    Map<Vector3i, Location<World>> roof = getClosestTo(Vector3i.UNIT_Y);
    roof.forEach((key, val) -> {
      if (val.equals(this.origin)) this.type = type;
      val.setBlockType(type);
    });
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void wall(BlockType type, Vector3i direction) {
    Map<Vector3i, Location<World>> wall = getClosestTo(direction);
    wall.forEach((key, val) -> {
      if (val.equals(this.origin)) this.type = type;
      val.setBlockType(type);
    });
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void floor (BlockType type) {
    Map<Vector3i, Location<World>> floor = getClosestTo(Vector3i.UNIT_Y.mul(-1));
    floor.forEach((key, val) -> {
      if (val.equals(this.origin)) this.type = type;
      val.setBlockType(type);
    });
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void deconstruct() {
    if (this.show) this.origin.setBlockType(this.type);
  }

  public void replace(BlockType type, BlockType with) {
    this.selection.forEach(loc -> {
      if (loc.getBlockType().equals(type)) {
        loc.setBlockType(with);
        if (loc.equals(this.origin)) this.type = with;
      }
    });
    if (this.show) this.origin.setBlockType(BlockTypes.BEACON);
  }

  public void half(Vector3i direction) {
    assert direction.length() == 1;
    assert Math.abs(direction.getX()) == 1 ^ Math.abs(direction.getY()) == 1 ^ Math.abs(direction.getZ()) == 1;

    Vector3i compare = this.origin.getBlockPosition().mul(direction.abs());
    boolean positive = direction.getMaxAxis() > 0;
    HashSet<Location<World>> clone = (HashSet<Location<World>>) this.selection.clone();
    this.selection.clear();
    clone.forEach(loc -> {
      Vector3i position = loc.getBlockPosition().mul(direction.abs());
      double compareVal = compare.getX() + compare.getY() + compare.getZ();
      double positionVal = position.getX() + position.getY() + position.getZ();
      if (positive && positionVal >= compareVal)
        addToSelection(loc);
      else if (!positive && positionVal <= compareVal)
        addToSelection(loc);
    });
  }


  private void addToSelection(Location<World> location) {
    try { this.selection.add(location); } catch (Exception ignored) {}
  }

  private void addCirclePoint(Vector2d x, Vector3i n) {
    if (n.equals(Vector3i.UNIT_X)) {
      addToSelection(this.origin.add(0, x.getX(), x.getY()));
      addToSelection(this.origin.add(0, -x.getX(), x.getY()));
      addToSelection(this.origin.add(0, x.getX(), -x.getY()));
      addToSelection(this.origin.add(0, -x.getX(), -x.getY()));
      addToSelection(this.origin.add(0, x.getY(), x.getX()));
      addToSelection(this.origin.add(0, -x.getY(), x.getX()));
      addToSelection(this.origin.add(0, x.getY(), -x.getX()));
      addToSelection(this.origin.add(0, -x.getY(), -x.getX()));
    }
    else if (n.equals(Vector3i.UNIT_Y)) {
      addToSelection(this.origin.add(x.getX(), 0, x.getY()));
      addToSelection(this.origin.add(-x.getX(), 0, x.getY()));
      addToSelection(this.origin.add(x.getX(), 0, -x.getY()));
      addToSelection(this.origin.add(-x.getX(), 0, -x.getY()));
      addToSelection(this.origin.add(x.getY(), 0, x.getX()));
      addToSelection(this.origin.add(-x.getY(), 0, x.getX()));
      addToSelection(this.origin.add(x.getY(), 0, -x.getX()));
      addToSelection(this.origin.add(-x.getY(), 0, -x.getX()));
    }
    else {
      addToSelection(this.origin.add(x.getX(), x.getY(), 0));
      addToSelection(this.origin.add(-x.getX(), x.getY(), 0));
      addToSelection(this.origin.add(x.getX(), -x.getY(), 0));
      addToSelection(this.origin.add(-x.getX(), -x.getY(), 0));
      addToSelection(this.origin.add(x.getY(), x.getX(), 0));
      addToSelection(this.origin.add(-x.getY(), x.getX(), 0));
      addToSelection(this.origin.add(x.getY(), -x.getX(), 0));
      addToSelection(this.origin.add(-x.getY(), -x.getX(), 0));
    }
  }

  private void addSpherePoint(Vector3i x) {
    addToSelection(this.origin.add(x.getX(),  x.getY(),  x.getZ()));
    addToSelection(this.origin.add(-x.getX(), x.getY(),  x.getZ()));
    addToSelection(this.origin.add(x.getX(),  -x.getY(), x.getZ()));
    addToSelection(this.origin.add(x.getX(),  x.getY(),  -x.getZ()));
    addToSelection(this.origin.add(-x.getX(), -x.getY(), x.getZ()));
    addToSelection(this.origin.add(x.getX(),  -x.getY(), -x.getZ()));
    addToSelection(this.origin.add(-x.getX(), x.getY(),  -x.getZ()));
    addToSelection(this.origin.add(-x.getX(), -x.getY(), -x.getZ()));

    addToSelection(this.origin.add(x.getX(),  x.getZ(),  x.getY()));
    addToSelection(this.origin.add(-x.getX(), x.getZ(),  x.getY()));
    addToSelection(this.origin.add(x.getX(),  x.getZ(),  -x.getY()));
    addToSelection(this.origin.add(x.getX(),  -x.getZ(), x.getY()));
    addToSelection(this.origin.add(-x.getX(), x.getZ(),  -x.getY()));
    addToSelection(this.origin.add(x.getX(),  -x.getZ(), -x.getY()));
    addToSelection(this.origin.add(-x.getX(), -x.getZ(), x.getY()));
    addToSelection(this.origin.add(-x.getX(), -x.getZ(), -x.getY()));

    addToSelection(this.origin.add(x.getY(),  x.getZ(),  x.getX()));
    addToSelection(this.origin.add(-x.getY(), x.getZ(),  x.getX()));
    addToSelection(this.origin.add(x.getY(),  -x.getZ(), x.getX()));
    addToSelection(this.origin.add(x.getY(),  x.getZ(),  -x.getX()));
    addToSelection(this.origin.add(-x.getY(), -x.getZ(), x.getX()));
    addToSelection(this.origin.add(x.getY(),  -x.getZ(), -x.getX()));
    addToSelection(this.origin.add(-x.getY(), x.getZ(),  -x.getX()));
    addToSelection(this.origin.add(-x.getY(), -x.getZ(), -x.getX()));

    addToSelection(this.origin.add(x.getZ(),  x.getY(),  x.getX()));
    addToSelection(this.origin.add(x.getZ(),  -x.getY(), x.getX()));
    addToSelection(this.origin.add(-x.getZ(), x.getY(),  x.getX()));
    addToSelection(this.origin.add(x.getZ(),  x.getY(),  -x.getX()));
    addToSelection(this.origin.add(-x.getZ(), -x.getY(), x.getX()));
    addToSelection(this.origin.add(-x.getZ(), x.getY(),  -x.getX()));
    addToSelection(this.origin.add(x.getZ(),  -x.getY(), -x.getX()));
    addToSelection(this.origin.add(-x.getZ(), -x.getY(), -x.getX()));

    addToSelection(this.origin.add(x.getZ(),  x.getX(),  x.getY()));
    addToSelection(this.origin.add(-x.getZ(), x.getX(),  x.getY()));
    addToSelection(this.origin.add(x.getZ(),  -x.getX(), x.getY()));
    addToSelection(this.origin.add(x.getZ(),  x.getX(),  -x.getY()));
    addToSelection(this.origin.add(-x.getZ(), -x.getX(), x.getY()));
    addToSelection(this.origin.add(x.getZ(),  -x.getX(), -x.getY()));
    addToSelection(this.origin.add(-x.getZ(), x.getX(),  -x.getY()));
    addToSelection(this.origin.add(-x.getZ(), -x.getX(), -x.getY()));

    addToSelection(this.origin.add(x.getY(),  x.getX(),  x.getZ()));
    addToSelection(this.origin.add(-x.getY(), x.getX(),  x.getZ()));
    addToSelection(this.origin.add(x.getY(),  -x.getX(), x.getZ()));
    addToSelection(this.origin.add(x.getY(),  x.getX(),  -x.getZ()));
    addToSelection(this.origin.add(-x.getY(), -x.getX(), x.getZ()));
    addToSelection(this.origin.add(x.getY(),  -x.getX(), -x.getZ()));
    addToSelection(this.origin.add(-x.getY(), x.getX(),  -x.getZ()));
    addToSelection(this.origin.add(-x.getY(), -x.getX(), -x.getZ()));
  }

  private Map<Vector3i, Location<World>> getClosestTo(Vector3i direction) {
    assert(direction.length() == 1);
    assert(Math.abs(direction.getX()) == 1 ^ Math.abs(direction.getY()) == 1 ^ Math.abs(direction.getZ()) == 1);

    Map<Vector3i, Location<World>> directionMost = new HashMap<>();
    this.selection.forEach(loc -> {
      Vector3i key = loc.getBlockPosition().cross(direction);
      if (!directionMost.containsKey(key)) directionMost.put(key, loc);

      Vector3i curr = directionMost.get(key).getBlockPosition().mul(direction);
      Vector3i test = loc.getBlockPosition().mul(direction);
      if (curr.getX() + curr.getY() + curr.getZ() < test.getX() + test.getY() + test.getZ())
        directionMost.put(key, loc);
    });
    return directionMost;
  }
}
