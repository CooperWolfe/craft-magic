package io.github.craftmagic.model;

import io.github.craftmagic.registries.MagicBlockTypeRegistryModule;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;

/**
 * An enumeration of all possible {@link BlockType}s in vanilla minecraft,
 * wrapped for easy naming
 */
@SuppressWarnings("unused")
public final class MagicBlockTypes {

  // SORTFIELDS: ON

  public static final MagicBlockType ACACIA_DOOR = createMagicBlock(BlockTypes.ACACIA_DOOR);

  public static final MagicBlockType ACACIA_FENCE = createMagicBlock(BlockTypes.ACACIA_FENCE);

  public static final MagicBlockType ACACIA_FENCE_GATE = createMagicBlock(BlockTypes.ACACIA_FENCE_GATE);

  public static final MagicBlockType ACACIA_STAIRS = createMagicBlock(BlockTypes.ACACIA_STAIRS);

  public static final MagicBlockType ACTIVATOR_RAIL = createMagicBlock(BlockTypes.ACTIVATOR_RAIL);

  public static final MagicBlockType AIR = createMagicBlock(BlockTypes.AIR);

  public static final MagicBlockType ANVIL = createMagicBlock(BlockTypes.ANVIL);

  public static final MagicBlockType BARRIER = createMagicBlock(BlockTypes.BARRIER);

  public static final MagicBlockType BEACON = createMagicBlock(BlockTypes.BEACON);

  public static final MagicBlockType BED = createMagicBlock(BlockTypes.BED);

  public static final MagicBlockType BEDROCK = createMagicBlock(BlockTypes.BEDROCK);

  public static final MagicBlockType BEETROOTS = createMagicBlock(BlockTypes.BEETROOTS);

  public static final MagicBlockType BIRCH_DOOR = createMagicBlock(BlockTypes.BIRCH_DOOR);

  public static final MagicBlockType BIRCH_FENCE = createMagicBlock(BlockTypes.BIRCH_FENCE);

  public static final MagicBlockType BIRCH_FENCE_GATE = createMagicBlock(BlockTypes.BIRCH_FENCE_GATE);

  public static final MagicBlockType BIRCH_STAIRS = createMagicBlock(BlockTypes.BIRCH_STAIRS);

  public static final MagicBlockType BLACK_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.BLACK_GLAZED_TERRACOTTA);

  public static final MagicBlockType BLACK_SHULKER_BOX = createMagicBlock(BlockTypes.BLACK_SHULKER_BOX);

  public static final MagicBlockType BLUE_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.BLUE_GLAZED_TERRACOTTA);

  public static final MagicBlockType BLUE_SHULKER_BOX = createMagicBlock(BlockTypes.BLUE_SHULKER_BOX);

  public static final MagicBlockType BONE_BLOCK = createMagicBlock(BlockTypes.BONE_BLOCK);

  public static final MagicBlockType BOOKSHELF = createMagicBlock(BlockTypes.BOOKSHELF);

  public static final MagicBlockType BREWING_STAND = createMagicBlock(BlockTypes.BREWING_STAND);

  public static final MagicBlockType BRICK_BLOCK = createMagicBlock(BlockTypes.BRICK_BLOCK);

  public static final MagicBlockType BRICK_STAIRS = createMagicBlock(BlockTypes.BRICK_STAIRS);

  public static final MagicBlockType BROWN_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.BROWN_GLAZED_TERRACOTTA);

  public static final MagicBlockType BROWN_MUSHROOM = createMagicBlock(BlockTypes.BROWN_MUSHROOM);

  public static final MagicBlockType BROWN_MUSHROOM_BLOCK = createMagicBlock(BlockTypes.BROWN_MUSHROOM_BLOCK);

  public static final MagicBlockType BROWN_SHULKER_BOX = createMagicBlock(BlockTypes.BROWN_SHULKER_BOX);

  public static final MagicBlockType CACTUS = createMagicBlock(BlockTypes.CACTUS);

  public static final MagicBlockType CAKE = createMagicBlock(BlockTypes.CAKE);

  public static final MagicBlockType CARPET = createMagicBlock(BlockTypes.CARPET);

  public static final MagicBlockType CARROTS = createMagicBlock(BlockTypes.CARROTS);

  public static final MagicBlockType CAULDRON = createMagicBlock(BlockTypes.CAULDRON);

  public static final MagicBlockType CHAIN_COMMAND_BLOCK = createMagicBlock(BlockTypes.CHAIN_COMMAND_BLOCK);

  public static final MagicBlockType CHEST = createMagicBlock(BlockTypes.CHEST);

  public static final MagicBlockType CHORUS_FLOWER = createMagicBlock(BlockTypes.CHORUS_FLOWER);

  public static final MagicBlockType CHORUS_PLANT = createMagicBlock(BlockTypes.CHORUS_PLANT);

  public static final MagicBlockType CLAY = createMagicBlock(BlockTypes.CLAY);

  public static final MagicBlockType COAL_BLOCK = createMagicBlock(BlockTypes.COAL_BLOCK);

  public static final MagicBlockType COAL_ORE = createMagicBlock(BlockTypes.COAL_ORE);

  public static final MagicBlockType COBBLESTONE = createMagicBlock(BlockTypes.COBBLESTONE);

  public static final MagicBlockType COBBLESTONE_WALL = createMagicBlock(BlockTypes.COBBLESTONE_WALL);

  public static final MagicBlockType COCOA = createMagicBlock(BlockTypes.COCOA);

  public static final MagicBlockType COMMAND_BLOCK = createMagicBlock(BlockTypes.COMMAND_BLOCK);

  public static final MagicBlockType CONCRETE = createMagicBlock(BlockTypes.CONCRETE);

  public static final MagicBlockType CONCRETE_POWDER = createMagicBlock(BlockTypes.CONCRETE_POWDER);

  public static final MagicBlockType CRAFTING_TABLE = createMagicBlock(BlockTypes.CRAFTING_TABLE);

  public static final MagicBlockType CYAN_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.CYAN_GLAZED_TERRACOTTA);

  public static final MagicBlockType CYAN_SHULKER_BOX = createMagicBlock(BlockTypes.CYAN_SHULKER_BOX);

  public static final MagicBlockType DARK_OAK_DOOR = createMagicBlock(BlockTypes.DARK_OAK_DOOR);

  public static final MagicBlockType DARK_OAK_FENCE = createMagicBlock(BlockTypes.DARK_OAK_FENCE);

  public static final MagicBlockType DARK_OAK_FENCE_GATE = createMagicBlock(BlockTypes.DARK_OAK_FENCE_GATE);

  public static final MagicBlockType DARK_OAK_STAIRS = createMagicBlock(BlockTypes.DARK_OAK_STAIRS);

  public static final MagicBlockType DAYLIGHT_DETECTOR = createMagicBlock(BlockTypes.DAYLIGHT_DETECTOR);

  public static final MagicBlockType DAYLIGHT_DETECTOR_INVERTED = createMagicBlock(BlockTypes.DAYLIGHT_DETECTOR_INVERTED);

  public static final MagicBlockType DEADBUSH = createMagicBlock(BlockTypes.DEADBUSH);

  public static final MagicBlockType DETECTOR_RAIL = createMagicBlock(BlockTypes.DETECTOR_RAIL);

  public static final MagicBlockType DIAMOND_BLOCK = createMagicBlock(BlockTypes.DIAMOND_BLOCK);

  public static final MagicBlockType DIAMOND_ORE = createMagicBlock(BlockTypes.DIAMOND_ORE);

  public static final MagicBlockType DIRT = createMagicBlock(BlockTypes.DIRT);

  public static final MagicBlockType DISPENSER = createMagicBlock(BlockTypes.DISPENSER);

  public static final MagicBlockType DOUBLE_PLANT = createMagicBlock(BlockTypes.DOUBLE_PLANT);

  public static final MagicBlockType DOUBLE_STONE_SLAB = createMagicBlock(BlockTypes.DOUBLE_STONE_SLAB);

  public static final MagicBlockType DOUBLE_STONE_SLAB2 = createMagicBlock(BlockTypes.DOUBLE_STONE_SLAB2);

  public static final MagicBlockType DOUBLE_WOODEN_SLAB = createMagicBlock(BlockTypes.DOUBLE_WOODEN_SLAB);

  public static final MagicBlockType DRAGON_EGG = createMagicBlock(BlockTypes.DRAGON_EGG);

  public static final MagicBlockType DROPPER = createMagicBlock(BlockTypes.DROPPER);

  public static final MagicBlockType EMERALD_BLOCK = createMagicBlock(BlockTypes.EMERALD_BLOCK);

  public static final MagicBlockType EMERALD_ORE = createMagicBlock(BlockTypes.EMERALD_ORE);

  public static final MagicBlockType ENCHANTING_TABLE = createMagicBlock(BlockTypes.ENCHANTING_TABLE);

  public static final MagicBlockType ENDER_CHEST = createMagicBlock(BlockTypes.ENDER_CHEST);

  public static final MagicBlockType END_BRICKS = createMagicBlock(BlockTypes.END_BRICKS);

  public static final MagicBlockType END_GATEWAY = createMagicBlock(BlockTypes.END_GATEWAY);

  public static final MagicBlockType END_PORTAL = createMagicBlock(BlockTypes.END_PORTAL);

  public static final MagicBlockType END_PORTAL_FRAME = createMagicBlock(BlockTypes.END_PORTAL_FRAME);

  public static final MagicBlockType END_ROD = createMagicBlock(BlockTypes.END_ROD);

  public static final MagicBlockType END_STONE = createMagicBlock(BlockTypes.END_STONE);

  public static final MagicBlockType FARMLAND = createMagicBlock(BlockTypes.FARMLAND);

  public static final MagicBlockType FENCE = createMagicBlock(BlockTypes.FENCE);

  public static final MagicBlockType FENCE_GATE = createMagicBlock(BlockTypes.FENCE_GATE);

  public static final MagicBlockType FIRE = createMagicBlock(BlockTypes.FIRE);

  public static final MagicBlockType FLOWER_POT = createMagicBlock(BlockTypes.FLOWER_POT);

  public static final MagicBlockType FLOWING_LAVA = createMagicBlock(BlockTypes.FLOWING_LAVA);

  public static final MagicBlockType FLOWING_WATER = createMagicBlock(BlockTypes.FLOWING_WATER);

  public static final MagicBlockType FROSTED_ICE = createMagicBlock(BlockTypes.FROSTED_ICE);

  public static final MagicBlockType FURNACE = createMagicBlock(BlockTypes.FURNACE);

  public static final MagicBlockType GLASS = createMagicBlock(BlockTypes.GLASS);

  public static final MagicBlockType GLASS_PANE = createMagicBlock(BlockTypes.GLASS_PANE);

  public static final MagicBlockType GLOWSTONE = createMagicBlock(BlockTypes.GLOWSTONE);

  public static final MagicBlockType GOLDEN_RAIL = createMagicBlock(BlockTypes.GOLDEN_RAIL);

  public static final MagicBlockType GOLD_BLOCK = createMagicBlock(BlockTypes.GOLD_BLOCK);

  public static final MagicBlockType GOLD_ORE = createMagicBlock(BlockTypes.GOLD_ORE);

  public static final MagicBlockType GRASS = createMagicBlock(BlockTypes.GRASS);

  public static final MagicBlockType GRASS_PATH = createMagicBlock(BlockTypes.GRASS_PATH);

  public static final MagicBlockType GRAVEL = createMagicBlock(BlockTypes.GRAVEL);

  public static final MagicBlockType GRAY_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.GRAY_GLAZED_TERRACOTTA);

  public static final MagicBlockType GRAY_SHULKER_BOX = createMagicBlock(BlockTypes.GRAY_SHULKER_BOX);

  public static final MagicBlockType GREEN_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.GREEN_GLAZED_TERRACOTTA);

  public static final MagicBlockType GREEN_SHULKER_BOX = createMagicBlock(BlockTypes.GREEN_SHULKER_BOX);

  public static final MagicBlockType HARDENED_CLAY = createMagicBlock(BlockTypes.HARDENED_CLAY);

  public static final MagicBlockType HAY_BLOCK = createMagicBlock(BlockTypes.HAY_BLOCK);

  public static final MagicBlockType HEAVY_WEIGHTED_PRESSURE_PLATE = createMagicBlock(BlockTypes.HEAVY_WEIGHTED_PRESSURE_PLATE);

  public static final MagicBlockType HOPPER = createMagicBlock(BlockTypes.HOPPER);

  public static final MagicBlockType ICE = createMagicBlock(BlockTypes.ICE);

  public static final MagicBlockType IRON_BARS = createMagicBlock(BlockTypes.IRON_BARS);

  public static final MagicBlockType IRON_BLOCK = createMagicBlock(BlockTypes.IRON_BLOCK);

  public static final MagicBlockType IRON_DOOR = createMagicBlock(BlockTypes.IRON_DOOR);

  public static final MagicBlockType IRON_ORE = createMagicBlock(BlockTypes.IRON_ORE);

  public static final MagicBlockType IRON_TRAPDOOR = createMagicBlock(BlockTypes.IRON_TRAPDOOR);

  public static final MagicBlockType JUKEBOX = createMagicBlock(BlockTypes.JUKEBOX);

  public static final MagicBlockType JUNGLE_DOOR = createMagicBlock(BlockTypes.JUNGLE_DOOR);

  public static final MagicBlockType JUNGLE_FENCE = createMagicBlock(BlockTypes.JUNGLE_FENCE);

  public static final MagicBlockType JUNGLE_FENCE_GATE = createMagicBlock(BlockTypes.JUNGLE_FENCE_GATE);

  public static final MagicBlockType JUNGLE_STAIRS = createMagicBlock(BlockTypes.JUNGLE_STAIRS);

  public static final MagicBlockType LADDER = createMagicBlock(BlockTypes.LADDER);

  public static final MagicBlockType LAPIS_BLOCK = createMagicBlock(BlockTypes.LAPIS_BLOCK);

  public static final MagicBlockType LAPIS_ORE = createMagicBlock(BlockTypes.LAPIS_ORE);

  public static final MagicBlockType LAVA = createMagicBlock(BlockTypes.LAVA);

  public static final MagicBlockType LEAVES = createMagicBlock(BlockTypes.LEAVES);

  public static final MagicBlockType LEAVES2 = createMagicBlock(BlockTypes.LEAVES2);

  public static final MagicBlockType LEVER = createMagicBlock(BlockTypes.LEVER);

  public static final MagicBlockType LIGHT_BLUE_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.LIGHT_BLUE_GLAZED_TERRACOTTA);

  public static final MagicBlockType LIGHT_BLUE_SHULKER_BOX = createMagicBlock(BlockTypes.LIGHT_BLUE_SHULKER_BOX);

  public static final MagicBlockType LIGHT_WEIGHTED_PRESSURE_PLATE = createMagicBlock(BlockTypes.LIGHT_WEIGHTED_PRESSURE_PLATE);

  public static final MagicBlockType LIME_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.LIME_GLAZED_TERRACOTTA);

  public static final MagicBlockType LIME_SHULKER_BOX = createMagicBlock(BlockTypes.LIME_SHULKER_BOX);

  public static final MagicBlockType LIT_FURNACE = createMagicBlock(BlockTypes.LIT_FURNACE);

  public static final MagicBlockType LIT_PUMPKIN = createMagicBlock(BlockTypes.LIT_PUMPKIN);

  public static final MagicBlockType LIT_REDSTONE_LAMP = createMagicBlock(BlockTypes.LIT_REDSTONE_LAMP);

  public static final MagicBlockType LIT_REDSTONE_ORE = createMagicBlock(BlockTypes.LIT_REDSTONE_ORE);

  public static final MagicBlockType LOG = createMagicBlock(BlockTypes.LOG);

  public static final MagicBlockType LOG2 = createMagicBlock(BlockTypes.LOG2);

  public static final MagicBlockType MAGENTA_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.MAGENTA_GLAZED_TERRACOTTA);

  public static final MagicBlockType MAGENTA_SHULKER_BOX = createMagicBlock(BlockTypes.MAGENTA_SHULKER_BOX);

  public static final MagicBlockType MAGMA = createMagicBlock(BlockTypes.MAGMA);

  public static final MagicBlockType MELON_BLOCK = createMagicBlock(BlockTypes.MELON_BLOCK);

  public static final MagicBlockType MELON_STEM = createMagicBlock(BlockTypes.MELON_STEM);

  public static final MagicBlockType MOB_SPAWNER = createMagicBlock(BlockTypes.MOB_SPAWNER);

  public static final MagicBlockType MONSTER_EGG = createMagicBlock(BlockTypes.MONSTER_EGG);

  public static final MagicBlockType MOSSY_COBBLESTONE = createMagicBlock(BlockTypes.MOSSY_COBBLESTONE);

  public static final MagicBlockType MYCELIUM = createMagicBlock(BlockTypes.MYCELIUM);

  public static final MagicBlockType NETHERRACK = createMagicBlock(BlockTypes.NETHERRACK);

  public static final MagicBlockType NETHER_BRICK = createMagicBlock(BlockTypes.NETHER_BRICK);

  public static final MagicBlockType NETHER_BRICK_FENCE = createMagicBlock(BlockTypes.NETHER_BRICK_FENCE);

  public static final MagicBlockType NETHER_BRICK_STAIRS = createMagicBlock(BlockTypes.NETHER_BRICK_STAIRS);

  public static final MagicBlockType NETHER_WART = createMagicBlock(BlockTypes.NETHER_WART);

  public static final MagicBlockType NETHER_WART_BLOCK = createMagicBlock(BlockTypes.NETHER_WART_BLOCK);

  public static final MagicBlockType NOTEBLOCK = createMagicBlock(BlockTypes.NOTEBLOCK);

  public static final MagicBlockType OAK_STAIRS = createMagicBlock(BlockTypes.OAK_STAIRS);

  public static final MagicBlockType OBSERVER = createMagicBlock(BlockTypes.OBSERVER);

  public static final MagicBlockType OBSIDIAN = createMagicBlock(BlockTypes.OBSIDIAN);

  public static final MagicBlockType ORANGE_SHULKER_BOX = createMagicBlock(BlockTypes.ORANGE_SHULKER_BOX);

  public static final MagicBlockType PACKED_ICE = createMagicBlock(BlockTypes.PACKED_ICE);

  public static final MagicBlockType PINK_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.PINK_GLAZED_TERRACOTTA);

  public static final MagicBlockType PINK_SHULKER_BOX = createMagicBlock(BlockTypes.PINK_SHULKER_BOX);

  public static final MagicBlockType PISTON = createMagicBlock(BlockTypes.PISTON);

  public static final MagicBlockType PISTON_EXTENSION = createMagicBlock(BlockTypes.PISTON_EXTENSION);

  public static final MagicBlockType PISTON_HEAD = createMagicBlock(BlockTypes.PISTON_HEAD);

  public static final MagicBlockType PLANKS = createMagicBlock(BlockTypes.PLANKS);

  public static final MagicBlockType PORTAL = createMagicBlock(BlockTypes.PORTAL);

  public static final MagicBlockType POTATOES = createMagicBlock(BlockTypes.POTATOES);

  public static final MagicBlockType POWERED_COMPARATOR = createMagicBlock(BlockTypes.POWERED_COMPARATOR);

  public static final MagicBlockType POWERED_REPEATER = createMagicBlock(BlockTypes.POWERED_REPEATER);

  public static final MagicBlockType PRISMARINE = createMagicBlock(BlockTypes.PRISMARINE);

  public static final MagicBlockType PUMPKIN = createMagicBlock(BlockTypes.PUMPKIN);

  public static final MagicBlockType PUMPKIN_STEM = createMagicBlock(BlockTypes.PUMPKIN_STEM);

  public static final MagicBlockType PURPLE_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.PURPLE_GLAZED_TERRACOTTA);

  public static final MagicBlockType PURPLE_SHULKER_BOX = createMagicBlock(BlockTypes.PURPLE_SHULKER_BOX);

  public static final MagicBlockType PURPUR_BLOCK = createMagicBlock(BlockTypes.PURPUR_BLOCK);

  public static final MagicBlockType PURPUR_DOUBLE_SLAB = createMagicBlock(BlockTypes.PURPUR_DOUBLE_SLAB);

  public static final MagicBlockType PURPUR_PILLAR = createMagicBlock(BlockTypes.PURPUR_PILLAR);

  public static final MagicBlockType PURPUR_SLAB = createMagicBlock(BlockTypes.PURPUR_SLAB);

  public static final MagicBlockType PURPUR_STAIRS = createMagicBlock(BlockTypes.PURPUR_STAIRS);

  public static final MagicBlockType QUARTZ_BLOCK = createMagicBlock(BlockTypes.QUARTZ_BLOCK);

  public static final MagicBlockType QUARTZ_ORE = createMagicBlock(BlockTypes.QUARTZ_ORE);

  public static final MagicBlockType QUARTZ_STAIRS = createMagicBlock(BlockTypes.QUARTZ_STAIRS);

  public static final MagicBlockType RAIL = createMagicBlock(BlockTypes.RAIL);

  public static final MagicBlockType REDSTONE_BLOCK = createMagicBlock(BlockTypes.REDSTONE_BLOCK);

  public static final MagicBlockType REDSTONE_LAMP = createMagicBlock(BlockTypes.REDSTONE_LAMP);

  public static final MagicBlockType REDSTONE_ORE = createMagicBlock(BlockTypes.REDSTONE_ORE);

  public static final MagicBlockType REDSTONE_TORCH = createMagicBlock(BlockTypes.REDSTONE_TORCH);

  public static final MagicBlockType REDSTONE_WIRE = createMagicBlock(BlockTypes.REDSTONE_WIRE);

  public static final MagicBlockType RED_FLOWER = createMagicBlock(BlockTypes.RED_FLOWER);

  public static final MagicBlockType RED_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.RED_GLAZED_TERRACOTTA);

  public static final MagicBlockType RED_MUSHROOM = createMagicBlock(BlockTypes.RED_MUSHROOM);

  public static final MagicBlockType RED_MUSHROOM_BLOCK = createMagicBlock(BlockTypes.RED_MUSHROOM_BLOCK);

  public static final MagicBlockType RED_NETHER_BRICK = createMagicBlock(BlockTypes.RED_NETHER_BRICK);

  public static final MagicBlockType RED_SANDSTONE = createMagicBlock(BlockTypes.RED_SANDSTONE);

  public static final MagicBlockType RED_SANDSTONE_STAIRS = createMagicBlock(BlockTypes.RED_SANDSTONE_STAIRS);

  public static final MagicBlockType RED_SHULKER_BOX = createMagicBlock(BlockTypes.RED_SHULKER_BOX);

  public static final MagicBlockType REEDS = createMagicBlock(BlockTypes.REEDS);

  public static final MagicBlockType REPEATING_COMMAND_BLOCK = createMagicBlock(BlockTypes.REPEATING_COMMAND_BLOCK);

  public static final MagicBlockType SAND = createMagicBlock(BlockTypes.SAND);

  public static final MagicBlockType SANDSTONE = createMagicBlock(BlockTypes.SANDSTONE);

  public static final MagicBlockType SANDSTONE_STAIRS = createMagicBlock(BlockTypes.SANDSTONE_STAIRS);

  public static final MagicBlockType SAPLING = createMagicBlock(BlockTypes.SAPLING);

  public static final MagicBlockType SEA_LANTERN = createMagicBlock(BlockTypes.SEA_LANTERN);

  public static final MagicBlockType SILVER_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.SILVER_GLAZED_TERRACOTTA);

  public static final MagicBlockType SILVER_SHULKER_BOX = createMagicBlock(BlockTypes.SILVER_SHULKER_BOX);

  public static final MagicBlockType SKULL = createMagicBlock(BlockTypes.SKULL);

  public static final MagicBlockType SLIME = createMagicBlock(BlockTypes.SLIME);

  public static final MagicBlockType SNOW = createMagicBlock(BlockTypes.SNOW);

  public static final MagicBlockType SNOW_LAYER = createMagicBlock(BlockTypes.SNOW_LAYER);

  public static final MagicBlockType SOUL_SAND = createMagicBlock(BlockTypes.SOUL_SAND);

  public static final MagicBlockType SPONGE = createMagicBlock(BlockTypes.SPONGE);

  public static final MagicBlockType SPRUCE_DOOR = createMagicBlock(BlockTypes.SPRUCE_DOOR);

  public static final MagicBlockType SPRUCE_FENCE = createMagicBlock(BlockTypes.SPRUCE_FENCE);

  public static final MagicBlockType SPRUCE_FENCE_GATE = createMagicBlock(BlockTypes.SPRUCE_FENCE_GATE);

  public static final MagicBlockType SPRUCE_STAIRS = createMagicBlock(BlockTypes.SPRUCE_STAIRS);

  public static final MagicBlockType STAINED_GLASS = createMagicBlock(BlockTypes.STAINED_GLASS);

  public static final MagicBlockType STAINED_GLASS_PANE = createMagicBlock(BlockTypes.STAINED_GLASS_PANE);

  public static final MagicBlockType STAINED_HARDENED_CLAY = createMagicBlock(BlockTypes.STAINED_HARDENED_CLAY);

  public static final MagicBlockType STANDING_BANNER = createMagicBlock(BlockTypes.STANDING_BANNER);

  public static final MagicBlockType STANDING_SIGN = createMagicBlock(BlockTypes.STANDING_SIGN);

  public static final MagicBlockType STICKY_PISTON = createMagicBlock(BlockTypes.STICKY_PISTON);

  public static final MagicBlockType STONE = createMagicBlock(BlockTypes.STONE);

  public static final MagicBlockType STONEBRICK = createMagicBlock(BlockTypes.STONEBRICK);

  public static final MagicBlockType STONE_BRICK_STAIRS = createMagicBlock(BlockTypes.STONE_BRICK_STAIRS);

  public static final MagicBlockType STONE_BUTTON = createMagicBlock(BlockTypes.STONE_BUTTON);

  public static final MagicBlockType STONE_PRESSURE_PLATE = createMagicBlock(BlockTypes.STONE_PRESSURE_PLATE);

  public static final MagicBlockType STONE_SLAB = createMagicBlock(BlockTypes.STONE_SLAB);

  public static final MagicBlockType STONE_SLAB2 = createMagicBlock(BlockTypes.STONE_SLAB2);

  public static final MagicBlockType STONE_STAIRS = createMagicBlock(BlockTypes.STONE_STAIRS);

  public static final MagicBlockType STRUCTURE_BLOCK = createMagicBlock(BlockTypes.STRUCTURE_BLOCK);

  public static final MagicBlockType STRUCTURE_VOID = createMagicBlock(BlockTypes.STRUCTURE_VOID);

  public static final MagicBlockType TALLGRASS = createMagicBlock(BlockTypes.TALLGRASS);

  public static final MagicBlockType TNT = createMagicBlock(BlockTypes.TNT);

  public static final MagicBlockType TORCH = createMagicBlock(BlockTypes.TORCH);

  public static final MagicBlockType TRAPDOOR = createMagicBlock(BlockTypes.TRAPDOOR);

  public static final MagicBlockType TRAPPED_CHEST = createMagicBlock(BlockTypes.TRAPPED_CHEST);

  public static final MagicBlockType TRIPWIRE = createMagicBlock(BlockTypes.TRIPWIRE);

  public static final MagicBlockType TRIPWIRE_HOOK = createMagicBlock(BlockTypes.TRIPWIRE_HOOK);

  public static final MagicBlockType UNLIT_REDSTONE_TORCH = createMagicBlock(BlockTypes.UNLIT_REDSTONE_TORCH);

  public static final MagicBlockType UNPOWERED_COMPARATOR = createMagicBlock(BlockTypes.UNPOWERED_COMPARATOR);

  public static final MagicBlockType UNPOWERED_REPEATER = createMagicBlock(BlockTypes.UNPOWERED_REPEATER);

  public static final MagicBlockType VINE = createMagicBlock(BlockTypes.VINE);

  public static final MagicBlockType WALL_BANNER = createMagicBlock(BlockTypes.WALL_BANNER);

  public static final MagicBlockType WALL_SIGN = createMagicBlock(BlockTypes.WALL_SIGN);

  public static final MagicBlockType WATER = createMagicBlock(BlockTypes.WATER);

  public static final MagicBlockType WATERLILY = createMagicBlock(BlockTypes.WATERLILY);

  public static final MagicBlockType WEB = createMagicBlock(BlockTypes.WEB);

  public static final MagicBlockType WHEAT = createMagicBlock(BlockTypes.WHEAT);

  public static final MagicBlockType WHITE_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.WHITE_GLAZED_TERRACOTTA);

  public static final MagicBlockType WHITE_SHULKER_BOX = createMagicBlock(BlockTypes.WHITE_SHULKER_BOX);

  public static final MagicBlockType WOODEN_BUTTON = createMagicBlock(BlockTypes.WOODEN_BUTTON);

  public static final MagicBlockType WOODEN_DOOR = createMagicBlock(BlockTypes.WOODEN_DOOR);

  public static final MagicBlockType WOODEN_PRESSURE_PLATE = createMagicBlock(BlockTypes.WOODEN_PRESSURE_PLATE);

  public static final MagicBlockType WOODEN_SLAB = createMagicBlock(BlockTypes.WOODEN_SLAB);

  public static final MagicBlockType WOOL = createMagicBlock(BlockTypes.WOOL);

  public static final MagicBlockType YELLOW_FLOWER = createMagicBlock(BlockTypes.YELLOW_FLOWER);

  public static final MagicBlockType YELLOW_GLAZED_TERRACOTTA = createMagicBlock(BlockTypes.YELLOW_GLAZED_TERRACOTTA);

  public static final MagicBlockType YELLOW_SHULKER_BOX = createMagicBlock(BlockTypes.YELLOW_SHULKER_BOX);

  // SORTFIELDS: OFF

  public MagicBlockTypes() {}

  private static MagicBlockType createMagicBlock(BlockType blockType) {
    return MagicBlockTypeRegistryModule.register(new MagicBlockType() {
      @Override
      public BlockType getBlockType() {
        return blockType;
      }

      @Override
      public String getId() {
        return blockType.getId().replace("minecraft:", "");
      }

      @Override
      public String getName() {
        return blockType.getName().replace("minecraft:", "");
      }
    });
  }

}
