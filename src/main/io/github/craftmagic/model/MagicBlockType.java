package io.github.craftmagic.model;

import org.spongepowered.api.CatalogType;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.util.annotation.CatalogedBy;

@CatalogedBy(MagicBlockTypes.class)
public interface MagicBlockType extends CatalogType {

  /**
   * Return the block type for the block.
   *
   * @return The blocktype
   */
  BlockType getBlockType();

}
