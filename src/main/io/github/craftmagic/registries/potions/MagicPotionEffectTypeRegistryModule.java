package io.github.craftmagic.registries.potions;

import io.github.craftmagic.model.potions.MagicPotionEffectType;
import org.spongepowered.api.registry.CatalogRegistryModule;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MagicPotionEffectTypeRegistryModule implements CatalogRegistryModule<MagicPotionEffectType> {

  private static final class Holder {
    static final MagicPotionEffectTypeRegistryModule INSTANCE = new MagicPotionEffectTypeRegistryModule();
  }
  public static MagicPotionEffectTypeRegistryModule getInstance() {
    return MagicPotionEffectTypeRegistryModule.Holder.INSTANCE;
  }
  public static MagicPotionEffectType register(MagicPotionEffectType magicPotionEffectType) {
    MagicPotionEffectTypeRegistryModule.getInstance().types
      .put(magicPotionEffectType.getId(), magicPotionEffectType);
    return magicPotionEffectType;
  }

  private Map<String, MagicPotionEffectType> types = new HashMap<>();

  @Override
  public Optional<MagicPotionEffectType> getById(String id) {
    MagicPotionEffectType type = types.get(id);
    if (type == null) return Optional.empty();
    return Optional.of(type);
  }

  @Override
  public Collection<MagicPotionEffectType> getAll() {
    return types.values();
  }

}
