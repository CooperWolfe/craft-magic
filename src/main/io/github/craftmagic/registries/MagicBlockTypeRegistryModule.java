package io.github.craftmagic.registries;

import io.github.craftmagic.model.MagicBlockType;
import org.spongepowered.api.registry.CatalogRegistryModule;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MagicBlockTypeRegistryModule implements CatalogRegistryModule<MagicBlockType> {

  private static final class Holder {
    static final MagicBlockTypeRegistryModule INSTANCE = new MagicBlockTypeRegistryModule();
  }
  public static MagicBlockTypeRegistryModule getInstance() {
    return Holder.INSTANCE;
  }
  public static MagicBlockType register(MagicBlockType magicBlockType) {
    MagicBlockTypeRegistryModule.getInstance().types
      .put(magicBlockType.getId(), magicBlockType);
    return magicBlockType;
  }


  private Map<String, MagicBlockType> types = new HashMap<>();

  @Override
  public Optional<MagicBlockType> getById(String id) {
    MagicBlockType type = types.get(id);
    if (type == null) return Optional.empty();
    return Optional.of(type);
  }

  @Override
  public Collection<MagicBlockType> getAll() {
    return types.values();
  }

}
