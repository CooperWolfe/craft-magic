package io.github.craftmagic.events;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Cancellable;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.impl.AbstractEvent;

public class CreateMagicSourceEvent extends AbstractEvent implements Cancellable {

  private BlockSnapshot targetBlock;
  private Player player;
  private boolean cancelled = false;

  public CreateMagicSourceEvent(BlockSnapshot targetBlock, Player player) {
    this.targetBlock = targetBlock;
    this.player = player;
  }

  public BlockSnapshot getTargetBlock() {
    return this.targetBlock;
  }

  @Override
  public boolean isCancelled() {
    return this.cancelled;
  }

  @Override
  public void setCancelled(boolean cancel) {
    this.cancelled = cancel;
  }

  @Override
  public Cause getCause() {
    return null;
  }

  public Player getPlayer() {
    return player;
  }
}
