package io.github.craftmagic.commands.selection;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class DeselectCommand implements CommandExecutor {

  private SourceService sourceService;

  public DeselectCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.selection.deselect")
      .description(Text.of("Returns magic source selections to their origin"))
      .executor(new DeselectCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    this.sourceService.deselect();
    src.sendMessage(Text.of("Deselected"));
    return CommandResult.success();
  }
}
