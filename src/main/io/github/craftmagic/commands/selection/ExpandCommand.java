package io.github.craftmagic.commands.selection;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.OrientationHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class ExpandCommand implements CommandExecutor {

  private SourceService sourceService;

  public ExpandCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.selection.expand")
      .description(Text.of("Expands selection a given distance in given direction"))
      .executor(new ExpandCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.direction("direction"),
        ArgHelper.integer("distance"),
        ArgHelper.flags(
          ArgHelper.magicBlockFlag("fillBlock", "f", "-fill"),
          ArgHelper.magicBlockFlag("outlineBlock", "o", "-outline"),
          ArgHelper.flag("x", "-exclude")
        )
      ).build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be player"));
    int distance = args.<Integer>getOne("distance").get();
    if (distance <= 0) throw new CommandException(Text.of("Distance must be positive"));
    Vector3i direction = args.<Vector3i>getOne("direction").orElse(Vector3i.UP);
    direction = OrientationHelper.getRelativeAxis((Player) src, direction);
    Optional<MagicBlockType> fill = args.getOne("fillBlock");
    Optional<MagicBlockType> outline =  args.getOne("outlineBlock");
    boolean exclude = args.hasAny("x");

    // Select and draw
    if (exclude) this.sourceService.deselect();
    this.sourceService.expand(distance, direction);
    fill.ifPresent(type -> this.sourceService.fill(type.getBlockType()));
    outline.ifPresent(type -> this.sourceService.outline(type.getBlockType()));

    // Message
    src.sendMessage(Text.of("Expanded selection by " + distance));

    // Success
    return CommandResult.success();
  }
}
