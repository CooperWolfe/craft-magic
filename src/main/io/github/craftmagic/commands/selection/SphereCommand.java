package io.github.craftmagic.commands.selection;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class SphereCommand implements CommandExecutor {

  private SourceService sourceService;

  public SphereCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.selection.sphere")
      .description(Text.of("Selects a sphere with specified radius"))
      .executor(new SphereCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.integer("radius"),
        ArgHelper.flags(
          ArgHelper.magicBlockFlag("fillBlock", "f", "-fill"),
          ArgHelper.magicBlockFlag("outlineBlock", "o", "-outline"),
          ArgHelper.flag("x", "-exclude")
        )
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    int radius = args.<Integer>getOne("radius").get();
    if (radius <= 0) throw new CommandException(Text.of("Radius must be > 0"));
    Optional<MagicBlockType> fill = args.getOne("fillBlock");
    Optional<MagicBlockType> outline =  args.getOne("outlineBlock");
    boolean exclude = args.hasAny("x");

    // Select and draw
    if (exclude) this.sourceService.deselect();
    this.sourceService.selectSphere(radius);
    fill.ifPresent(type -> this.sourceService.fill(type.getBlockType()));
    outline.ifPresent(type -> this.sourceService.outline(type.getBlockType()));

    // Message
    src.sendMessage(Text.of((fill.isPresent() || outline.isPresent() ? "Drew" : "Selected") + " sphere of radius " + radius));

    // Success
    return CommandResult.success();
  }
}
