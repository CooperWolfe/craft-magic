package io.github.craftmagic.commands.selection;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.OrientationHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class SquareCommand implements CommandExecutor {

  private SourceService sourceService;

  public SquareCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.selection.square")
      .description(Text.of("Selects a square with a radius and plane"))
      .executor(new SquareCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.integer("radius"),
        ArgHelper.plane("plane", true),
        ArgHelper.flags(
          ArgHelper.magicBlockFlag("fillBlock", "f", "-fill"),
          ArgHelper.magicBlockFlag("outlineBlock", "o", "-outline"),
          ArgHelper.flag("x", "-exclude")
        )
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be a player"));
    int radius = args.<Integer>getOne("radius").get();
    if (radius <= 0) throw new CommandException(Text.of("Radius must be positive"));
    Vector3i plane = args.<Vector3i>getOne("plane").orElse(Vector3i.UP);
    Vector3i normal = OrientationHelper.getRelativeAxis((Player) src, plane).abs();
    Optional<MagicBlockType> fill = args.getOne("fillBlock");
    Optional<MagicBlockType> outline =  args.getOne("outlineBlock");
    boolean exclude = args.hasAny("x");

    // Select and draw
    if (exclude) this.sourceService.deselect();
    this.sourceService.selectSquare(radius, normal);
    fill.ifPresent(type -> this.sourceService.fill(type.getBlockType()));
    outline.ifPresent(type -> this.sourceService.outline(type.getBlockType()));

    // Message
    src.sendMessage(Text.of((fill.isPresent() || outline.isPresent() ? "Drew" : "Selected") + " square of radius " + radius));

    // Success
    return CommandResult.success();
  }
}
