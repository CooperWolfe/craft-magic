package io.github.craftmagic.commands.selection;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.OrientationHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.sources.MagicSource;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public class MoveCommand implements CommandExecutor {

  private SourceService sourceService;

  public MoveCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.selection.move")
      .description(Text.of("Moves selected or all sources"))
      .executor(new MoveCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.optional(
          ArgHelper.direction("direction"),
          ArgHelper.integer("distance")
        ),
        ArgHelper.flags(
          ArgHelper.flag("r", "-release"),
          ArgHelper.flag("k", "-keep"),
          ArgHelper.flag("p", "-persist"),
          ArgHelper.vector3dFlag("translation", "v", "-vector")
        )
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be a player"));
    if (!args.hasAny("translation") && !args.hasAny("direction"))
      throw new CommandException(Text.of("Direction and distance or translation vector required"));
    Vector3i translation = args.<Vector3d>getOne("translation").orElseGet(() -> {
      Vector3i direction = args.<Vector3i>getOne("direction").get();
      direction = OrientationHelper.getRelativeAxis((Player) src, direction);
      int distance = args.<Integer>getOne("distance").get();
      return direction.mul(distance).toDouble();
    }).toInt();
    boolean release = args.hasAny("r");
    boolean keep = args.hasAny("k");
    boolean persist = args.hasAny("p");

    // Move
    List<MagicSource> sources = this.sourceService.getSources();
    for (MagicSource source : sources) {
      Location<World> oldLocation = source.getOrigin();
      BlockType type = source.getType();

      if (release) source.fill(BlockTypes.AIR);

      source.move(translation);
      if (keep) this.sourceService.addSource(oldLocation);

      if (persist && !type.equals(BlockTypes.AIR)) source.fill(type);
    }

    // Send message
    src.sendMessage(Text.of("Moved sources by " + translation));

    // Success
    return CommandResult.success();
  }
}
