package io.github.craftmagic.commands;

import io.github.craftmagic.commands.draw.*;
import io.github.craftmagic.commands.selection.*;
import io.github.craftmagic.commands.sources.SourcesCommand;
import io.github.craftmagic.commands.potions.PotionCommand;
import io.github.craftmagic.commands.spells.TeleportCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class CraftMagicCommand implements CommandExecutor {

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .executor(new CraftMagicCommand())
      .description(Text.of("Root command for all CraftMagic commands"))
      .permission("craftmagic.root")

      // MagicSource
      // Draw
      .child(ClearCommand.getCommandSpec(), "c", "cl", "cls", "clear")
      .child(FillCommand.getCommandSpec(), "f", "fill")
      .child(FloorCommand.getCommandSpec(), "flr", "floor")
      .child(OutlineCommand.getCommandSpec(), "o", "outline")
      .child(ReplaceCommand.getCommandSpec(), "r", "rp", "rplc", "replace")
      .child(RoofCommand.getCommandSpec(), "rf", "roof")
      .child(WallCommand.getCommandSpec(), "w", "wall")
      .child(WallsCommand.getCommandSpec(), "ws", "walls")
      // Selection
      .child(CircleCommand.getCommandSpec(), "cc", "ccl", "crcl", "circle")
      .child(CubeCommand.getCommandSpec(), "q", "qb", "cb", "cube")
      .child(DeselectCommand.getCommandSpec(), "d", "dsl", "deselect")
      .child(ExpandCommand.getCommandSpec(), "e", "exp", "expand")
      .child(HalfCommand.getCommandSpec(), "h", "hf", "half")
      .child(MoveCommand.getCommandSpec(), "m", "mv", "move")
      .child(SphereCommand.getCommandSpec(), "sph", "sphere")
      .child(SquareCommand.getCommandSpec(), "sq", "sqr", "square")
      // Sources
      .child(SourcesCommand.getCommandSpec(), "s", "src", "source", "sources")

      // Potions
      .child(PotionCommand.getCommandSpec(), "p", "pn", "potion")

      // Spells
      .child(TeleportCommand.getCommandSpec(), "t", "tl", "teleport")

      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    throw new CommandException(Text.of("Command expected: /cm <src|dsl|f|o|cl|q|e|sph|sq|m>"));
  }
}
