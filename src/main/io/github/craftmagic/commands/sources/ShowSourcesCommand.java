package io.github.craftmagic.commands.sources;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class ShowSourcesCommand implements CommandExecutor {

  private SourceService sourceService;

  public ShowSourcesCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources.show")
      .description(Text.of("Turns magic sources to beacons"))
      .executor(new ShowSourcesCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    this.sourceService.show();
    src.sendMessage(Text.of("Magic sources are now beacons"));
    return CommandResult.success();
  }
}
