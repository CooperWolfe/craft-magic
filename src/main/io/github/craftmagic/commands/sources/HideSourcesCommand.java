package io.github.craftmagic.commands.sources;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class HideSourcesCommand implements CommandExecutor {

  private SourceService sourceService;

  public HideSourcesCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources.hide")
      .description(Text.of("Hides magic sources"))
      .executor(new HideSourcesCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    this.sourceService.hide();
    src.sendMessage(Text.of("Magic sources hidden"));
    return CommandResult.success();
  }
}
