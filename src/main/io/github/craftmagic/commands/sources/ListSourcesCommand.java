package io.github.craftmagic.commands.sources;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.sources.MagicSource;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

import java.util.List;

public class ListSourcesCommand implements CommandExecutor {

  private SourceService sourceService;

  public ListSourcesCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources.list")
      .description(Text.of("Lists magic sources"))
      .executor(new ListSourcesCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    List<MagicSource> sources = this.sourceService.getSources();
    for (MagicSource source : sources) src.sendMessage(Text.of(source.getOrigin().getPosition().toString()));
    return CommandResult.success();
  }
}
