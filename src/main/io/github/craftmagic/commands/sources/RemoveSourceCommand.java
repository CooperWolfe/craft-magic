package io.github.craftmagic.commands.sources;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class RemoveSourceCommand implements CommandExecutor {

  private SourceService sourceService;

  public RemoveSourceCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources.remove")
      .description(Text.of("Removes source at an index"))
      .executor(new RemoveSourceCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(ArgHelper.integer("index", true))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    if (!args.hasAny("index")) ClearSourcesCommand.getCommandSpec().getExecutor().execute(src, args);
    int index = args.<Integer>getOne("index").get();
    Location<World> location = this.sourceService.removeSource(index).getOrigin();
    src.sendMessage(Text.of("Removed magic source at " + location.getPosition()));
    return null;
  }
}
