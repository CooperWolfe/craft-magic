package io.github.craftmagic.commands.sources;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class SourcesCommand implements CommandExecutor {

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources")
      .description(Text.of("Root command for managing magic sources"))
      .executor(new SourcesCommand())
      .child(ClearSourcesCommand.getCommandSpec(), "c", "cls", "clear")
      .child(ListSourcesCommand.getCommandSpec(), "l", "ls", "list")
      .child(RemoveSourceCommand.getCommandSpec(), "r", "rm", "remove")
      .child(ShowSourcesCommand.getCommandSpec(), "s", "show")
      .child(HideSourcesCommand.getCommandSpec(), "h", "hide")
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    return null;
  }
}
