package io.github.craftmagic.commands.sources;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class ClearSourcesCommand implements CommandExecutor {

  private SourceService sourceService;

  public ClearSourcesCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.sources.clear")
      .description(Text.of("Clears magic sources"))
      .executor(new ClearSourcesCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    this.sourceService.clearSources();
    src.sendMessage(Text.of("All sources cleared"));
    return CommandResult.success();
  }
}
