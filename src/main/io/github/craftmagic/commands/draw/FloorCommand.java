package io.github.craftmagic.commands.draw;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.model.MagicBlockTypes;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class FloorCommand implements CommandExecutor {

  private SourceService sourceService;

  public FloorCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.draw.floor")
      .description(Text.of("Draws floor of selection"))
      .executor(new FloorCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(ArgHelper.magicBlock("block", true))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    MagicBlockType type = args.<MagicBlockType>getOne("block").orElse(MagicBlockTypes.AIR);

    // Draw
    this.sourceService.floor(type.getBlockType());

    // Message
    src.sendMessage(Text.of("Floor drawn"));

    // Success
    return CommandResult.success();
  }
}
