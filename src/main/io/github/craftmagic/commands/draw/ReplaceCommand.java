package io.github.craftmagic.commands.draw;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class ReplaceCommand implements CommandExecutor {

  private SourceService sourceService;

  public ReplaceCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.draw.replace")
      .description(Text.of("Replaces one type with another"))
      .executor(new ReplaceCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.magicBlock("type"),
        ArgHelper.magicBlock("with")
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    MagicBlockType type = args.<MagicBlockType>getOne("type").get();
    MagicBlockType with = args.<MagicBlockType>getOne("with").get();
    this.sourceService.replace(type.getBlockType(), with.getBlockType());
    src.sendMessage(Text.of("Replaced"));
    return CommandResult.success();
  }
}
