package io.github.craftmagic.commands.draw;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.model.MagicBlockTypes;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class FillCommand implements CommandExecutor {

  private SourceService sourceService;

  public FillCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.draw.fill")
      .description(Text.of("Fills the selected area with specified block"))
      .executor(new FillCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(ArgHelper.magicBlock("block", true))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    MagicBlockType block = args.<MagicBlockType>getOne("block").orElse(MagicBlockTypes.AIR);
    this.sourceService.fill(block.getBlockType());
    src.sendMessage(Text.of("Filled selection"));
    return CommandResult.success();
  }
}
