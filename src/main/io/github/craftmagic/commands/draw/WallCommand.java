package io.github.craftmagic.commands.draw;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.OrientationHelper;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.model.MagicBlockTypes;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class WallCommand implements CommandExecutor {

  private SourceService sourceService;

  public WallCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.draw.wall")
      .description(Text.of("Draws one wall"))
      .executor(new WallCommand(ServiceHelper.getService(SourceService.class)))
      .arguments(
        ArgHelper.side("side"),
        ArgHelper.magicBlock("block", true)
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

    // Get arguments
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be a player"));
    Vector3i direction = args.<Vector3i>getOne("side").get();
    direction = OrientationHelper.getRelativeAxis((Player) src, direction);
    MagicBlockType block = args.<MagicBlockType>getOne("block").orElse(MagicBlockTypes.AIR);

    // Draw
    this.sourceService.wall(direction, block.getBlockType());

    // Message
    src.sendMessage(Text.of("Wall drawn"));

    // Success
    return CommandResult.success();
  }
}
