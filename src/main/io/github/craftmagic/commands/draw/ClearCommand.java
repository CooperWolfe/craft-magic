package io.github.craftmagic.commands.draw;

import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class ClearCommand implements CommandExecutor {

  private SourceService sourceService;

  public ClearCommand(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.draw.clear")
      .description(Text.of("Clears selection"))
      .executor(new ClearCommand(ServiceHelper.getService(SourceService.class)))
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    this.sourceService.fill(BlockTypes.AIR);
    src.sendMessage(Text.of("Cleared selection"));
    return CommandResult.success();
  }
}
