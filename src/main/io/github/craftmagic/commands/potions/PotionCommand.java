package io.github.craftmagic.commands.potions;

import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.model.potions.MagicPotionEffectType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.manipulator.mutable.PotionEffectData;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class PotionCommand implements CommandExecutor {

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.potions")
      .description(Text.of("Gives user a potion's effects for infinite time"))
      .executor(new PotionCommand())
      .arguments(
        ArgHelper.potion("effect"),
        ArgHelper.flags(ArgHelper.flag("x", "-exclude"))
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    MagicPotionEffectType type = args.<MagicPotionEffectType>getOne("effect").get();
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be a player."));
    Player player = (Player) src;
    PotionEffectData effects = player.getOrCreate(PotionEffectData.class).get();
    PotionEffect effect = PotionEffect.builder()
      .potionType(type.getPotionEffectType())
      .amplifier(Integer.MAX_VALUE)
      .duration(Integer.MAX_VALUE)
      .ambience(true)
      .build();

    if (args.hasAny("x")) {
      int len = effects.getValues().size();
      for (int i = 0; i < len; ++i) {
        if (effects.get(i).get().getType().getId().equals(effect.getType().getId())) {
          effects.remove(i);
          break;
        }
      }
    }
    else
      effects.addElement(effect);

    player.offer(effects);
    return CommandResult.success();
  }
}
