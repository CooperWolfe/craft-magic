package io.github.craftmagic.commands.spells;

import com.flowpowered.math.vector.Vector3i;
import io.github.craftmagic.helpers.ArgHelper;
import io.github.craftmagic.helpers.OrientationHelper;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.text.Text;

public class TeleportCommand implements CommandExecutor {

  public static CommandSpec getCommandSpec() throws ProvisioningException {
    return CommandSpec.builder()
      .permission("craftmagic.spells.teleport")
      .description(Text.of("Teleports the user to the next air space"))
      .executor(new TeleportCommand())
      .arguments(
        ArgHelper.direction("direction")
      )
      .build();
  }

  @Override
  public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
    if (!(src instanceof Player)) throw new CommandException(Text.of("src must be a player"));
    Player player = (Player) src;
    Vector3i playerPosition = player.getLocation().getBlockPosition();
    Vector3i direction = args.<Vector3i>getOne("direction").get();

    for (int i = 0; true; ++i) {
      Vector3i offset = OrientationHelper.getRelativeAxis(player, direction).mul(i);
      Vector3i location = offset.add(playerPosition);
      BlockType blockType = player.getWorld().getBlockType(location);

      if (!blockType.equals(BlockTypes.AIR)) {
        for (int j = i + 1; true; ++j) {
          offset = OrientationHelper.getRelativeAxis(player, direction).mul(j);
          location = offset.add(playerPosition);
          blockType = player.getWorld().getBlockType(location);

          if (blockType.equals(BlockTypes.AIR)) {
            player.setLocation(player.getWorld().getLocation(location));
            break;
          }
          if (outOfBounds(player.getWorld().getBlockMax(), location))
            throw new CommandException(Text.of("Cannot teleport in that direction"));
        }
        break;
      }
      if (outOfBounds(player.getWorld().getBlockMax(), location))
        throw new CommandException(Text.of("Cannot teleport in that direction"));
    }

    return CommandResult.success();
  }

  private boolean outOfBounds(Vector3i max, Vector3i position) {
    int x = position.getX(), y = position.getY(), z = position.getZ(),
      maxX = max.getX(), maxY = max.getY(), maxZ = max.getZ();
    return x < -maxX || x > maxX
        || y < -maxY || y > maxY
        || z < -maxZ || z > maxZ;
  }
}
