package io.github.craftmagic.listeners.sources;

import com.google.inject.Inject;
import io.github.craftmagic.events.CreateMagicSourceEvent;
import io.github.craftmagic.services.sources.SourceService;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class MagicCreateMagicSourceListener implements EventListener<CreateMagicSourceEvent> {

  private SourceService sourceService;

  @Inject
  public MagicCreateMagicSourceListener(SourceService sourceService) {
    this.sourceService = sourceService;
  }

  @Override
  @Listener
  public void handle(CreateMagicSourceEvent event) {
    if (event.isCancelled()) return;
    Optional<Location<World>> location = event.getTargetBlock().getLocation();
    if (!location.isPresent()) return;
    Player player = event.getPlayer();

    if (this.sourceService.addSource(location.get()) != null)
      player.sendMessage(Text.of("Added: " + location.get().getPosition()));
    else
      player.sendMessage(Text.of(location.get().getPosition().toString()));
  }
}
