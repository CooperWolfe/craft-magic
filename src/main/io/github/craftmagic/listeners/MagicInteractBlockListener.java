package io.github.craftmagic.listeners;

import com.google.inject.Inject;
import io.github.craftmagic.events.CreateMagicSourceEvent;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.EventManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;

import java.util.Optional;

public class MagicInteractBlockListener implements EventListener<InteractBlockEvent> {

  private EventManager eventManager;

  @Inject
  public MagicInteractBlockListener(EventManager eventManager) {
    this.eventManager = eventManager;
  }

  @Override
  @Listener
  public void handle(InteractBlockEvent event) {
    if (!(event instanceof InteractBlockEvent.Secondary.OffHand)) return;
    if (event.isCancelled()) return;
    Optional<Player> player = event.getCause().first(Player.class);
    if (!player.isPresent()) return;
    Optional<ItemStack> equippedItem = player.get().getEquipped(EquipmentTypes.MAIN_HAND);
    if (equippedItem.isPresent()) return;

    eventManager.post(new CreateMagicSourceEvent(event.getTargetBlock(), player.get()));
    event.setCancelled(true);
  }
}
