package io.github.craftmagic;

import com.google.inject.Inject;
import io.github.craftmagic.commands.CraftMagicCommand;
import io.github.craftmagic.helpers.ServiceHelper;
import io.github.craftmagic.listeners.MagicInteractBlockListener;
import io.github.craftmagic.model.MagicBlockType;
import io.github.craftmagic.model.MagicBlockTypes;
import io.github.craftmagic.registries.MagicBlockTypeRegistryModule;
import io.github.craftmagic.events.CreateMagicSourceEvent;
import io.github.craftmagic.listeners.sources.MagicCreateMagicSourceListener;
import io.github.craftmagic.services.sources.MagicSourceService;
import io.github.craftmagic.services.sources.SourceService;
import io.github.craftmagic.model.potions.MagicPotionEffectType;
import io.github.craftmagic.model.potions.MagicPotionEffectTypes;
import io.github.craftmagic.registries.potions.MagicPotionEffectTypeRegistryModule;
import org.slf4j.Logger;
import org.spongepowered.api.GameRegistry;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.event.EventManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.service.ServiceManager;

@Plugin(id = "craftmagic", name = "Craft Magic", version = "0.0.1")
public class CraftMagicPlugin {

  private Logger logger;
  private EventManager eventManager;
  private ServiceManager serviceManager;
  private CommandManager commandManager;
  private GameRegistry gameRegistry;

  @Inject
  public CraftMagicPlugin(Logger logger) {
    this.logger = logger;
    this.eventManager = Sponge.getEventManager();
    this.serviceManager = Sponge.getServiceManager();
    this.commandManager = Sponge.getCommandManager();
    this.gameRegistry = Sponge.getRegistry();
  }

  @Listener
  public void onInitialization(GameInitializationEvent event) {
    try {
      this.registerServices();
      this.registerListeners();
      this.registerCommands();
      this.registerGame();
    } catch (ProvisioningException e) {
      this.logger.error("Error during initialization. Exiting.", e);
      System.exit(1);
    }
  }

  private void registerServices() throws ProvisioningException {
    this.serviceManager.setProvider(
      this,
      SourceService.class,
      new MagicSourceService()
    );
  }
  private void registerListeners() throws ProvisioningException {
    this.eventManager.registerListener(
      this,
      InteractBlockEvent.class,
      new MagicInteractBlockListener(this.eventManager)
    );
    this.eventManager.registerListener(
      this,
      CreateMagicSourceEvent.class,
      new MagicCreateMagicSourceListener(ServiceHelper.getService(SourceService.class))
    );
  }
  private void registerCommands() throws ProvisioningException {
    this.commandManager.register(this, CraftMagicCommand.getCommandSpec(), "cm", "craftmagic");
  }
  private void registerGame() throws ProvisioningException {
    new MagicBlockTypes();
    new MagicPotionEffectTypes();
    this.gameRegistry.registerModule(MagicBlockType.class, MagicBlockTypeRegistryModule.getInstance());
    this.gameRegistry.registerModule(MagicPotionEffectType.class, MagicPotionEffectTypeRegistryModule.getInstance());
  }
}
